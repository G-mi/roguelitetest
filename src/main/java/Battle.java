import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Battle {

    private final ArrayList<Ally> battlers;
    private final Monster monster;


    /*
        @arg allies should be sorted by speed attribute. this is not a must, and can be changed based on how the game should work
     */
    public Battle(Monster monster, ArrayList<Ally> battlers) {
        if (monster == null || battlers == null || battlers.size() > 4) throw new IllegalArgumentException();
        this.battlers = battlers;
        this.monster = monster;

    }

    ArrayList<Ally> getBattlers() {
        return battlers;
    }

    Monster getMonster() {
        return monster;
    }

    public int battle(Scanner scanner) {
        while (!battlers.isEmpty() && monster.isAlive()) {
            for (int i = 0; i < battlers.size(); i++) {
                // System.out.println(battlers.get(i).toString() + " attacks");
                // System.out.println("0: melee 1: magic(not implemented) 2: defend 3: drink potion");
                if (battlers.get(i).hasStatusEffect()) {
                    battlers.get(i).applyStatusEffect();
                } battleActions(battlers.get(i), scanner.nextInt());
            }            //System.out.println("Dragon Attacks");
            if (monster.hasStatusEffect())
                monster.applyStatusEffect();
            randomizedDragonAttack();

            battlers.removeIf(ally -> ally.getCurrentHP()==0);



        }
        if (battlers.isEmpty()) return 0;
        else return 1;
    }

    void usePotion(Ally ally, Item.Potion potion) {
        ally.drinkPotion(potion);
    }


    void allyTakeDamage(Ally ally, int damage) {
        for (int i = 0; i < battlers.size(); i++) {
            if (battlers.get(i).equals(ally)) {

                ally.takeDamage(damage);
                if (ally.getCurrentHP() == 0) {
                    battlers.remove(ally);
                }

            }
        }
    }

    void randomizedDragonAttack() {
        Dragon d = (Dragon) monster;
        monsterAttack(d.aiAttack());
    }

    void monsterAttack(int i) {
        if (monster.getClass() == Dragon.class) {
            Dragon dragon = (Dragon) monster;
            switch (i) {
                case 0:
                    dragon.throwFireBall(battlers.get(0));
                    break;
                case 1:
                    dragon.castBlizzard(battlers.get(0));
                    break;
                case 2:
                    dragon.throwMultiFireBall(battlers);
                    break;
                case 3:
                    dragon.ironTailAttack(battlers.get(0));
                    break;
                default:
                    throw new IllegalArgumentException();

            }

        }

    }


    void battleActions(Ally ally, int i) {
        ally.raiseDefence(false);
        if (ally.getClass() == Warrior.class) {
            ((Warrior) ally).setCountering(false);
        }

        switch (i) {
            case 0:
                ally.attackWithWeapon(monster);
                break;
            case 1:
                doClassAction(ally);
                break;
            case 2:
                ally.raiseDefence(true);
                break;
            case 3:
                Item.Potion potion = getPotionFromInventory();
                ally.drinkPotion(potion);
                break;
        }

    }

    private void doClassAction(Ally ally) {
        switch (ally.getClass().getName()) {
            case "Warrior":
                ((Warrior) ally).setCountering(true);
                break;

            case "Priest":
                // getInputOnWhomtoHealFromPlayer?
                ((Priest) ally).castHeal(battlers.get(0));
                break;

            case "Rogue":
                 ((Rogue)ally).steal(monster, new Random());
                break;

            case "Mage":
                useMagic((Mage) ally);
                break;
        }
    }

    private void useMagic(Mage mage) {
        //get some sort of input from user here
         int input = 0;
         switch (input){
             case 0:
                 mage.castBlizzard(monster);
                 break;
             case 1:
           //      mage.castWind(monster);
                 break;
         }

    }

    private Item.Potion getPotionFromInventory() {
        //todo if Henrik needs this to actually take from inventory, do that using user input
        return Item.Potion.HealthPotion;
    }


}
