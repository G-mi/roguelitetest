public class Warrior extends Ally {

    private boolean counter;
    private static final int COUNTER_STAMINA_COST = 4;


    public Warrior(String name, int totalExperience, Equipment equipment, Race race){
        super (name, totalExperience, equipment, race, 6, 2, 5,
                4, 1, 1);
    }

    public boolean isCountering(){
        return counter;
    }

    public void setCountering(boolean countering){
        counter = countering;
    }

    public int getCOUNTER_STAMINA_COST(){
        return COUNTER_STAMINA_COST;
    }

    void counter(Monster monster){
        if (getCurrentStamina() >= getCOUNTER_STAMINA_COST()){
            useStamina(getCOUNTER_STAMINA_COST());
            attackWithWeapon(monster);
        }

    }


    public void takeDamage(int i, Monster attacker) {
        if(counter){
            counter(attacker);
        }else {
            takeDamage(i);
        }


    }
}
