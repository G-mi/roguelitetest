import java.util.*;


public class Dragon extends Monster{
    private static final int dropAmount = 2;
    private static final ArrayList<Item> droppables = new ArrayList<Item>(){
        {
            add(new Item(Item.Armour.OrichalcumArmour));
            add(new Item(Item.Weapon.Spear));
            add(new Item(Item.Potion.HealthPotion));
            add(new Item(Item.Potion.StaminaPotion));
        }
    };

    private static final int FIRE_BALL_DEFAULT_MANA = 50;

    private static final int IRON_TAIL_DAMAGE = 25;

    private static final int WIND_DAMAGE_TAKEN_QUANTIFIER = 2;
    private static final double EARTH_DAMAGE_TAKEN_QUANTIFIER = 0.8;
    private static final double ICE_DAMAGE_TAKEN_QUANTIFIER = 0.6;
    private static final int TSUNAMI_DAMAGE_TAKEN_QUANTIFIER = 0;

    private static final Random random = new Random();

    private final Set<Element> resilience, weakness;

    private final OffensiveMagic fireBall;
    private final OffensiveMagic blizzard;

    public Dragon(String name, int maxHP, int maxMP, int maxStamina) {
        super(name, maxHP, maxMP, maxStamina, droppables, dropAmount);

        resilience = new HashSet<>(Arrays.asList(Element.EARTH, Element.FIRE, Element.ICE));

        weakness = new HashSet<>(Arrays.asList(Element.WATER, Element.WIND));

        fireBall = new OffensiveMagic(OffensiveMagic.SpellType.FIREBALL, FIRE_BALL_DEFAULT_MANA);
        blizzard = new OffensiveMagic(OffensiveMagic.SpellType.BLIZZARD, FREEZE_DEFAULT_MANA);

    }

    public void ironTailAttack(Character target) {
        if (target == null) {
            throw new IllegalArgumentException();
        }
        dealDamage(target, IRON_TAIL_DAMAGE);
    }

    public void attackWithSpell(Character target, OffensiveMagic offensiveSpell) {
        if (target == null) {
            throw new IllegalArgumentException();
        }
        dealDamage(target, offensiveSpell);
    }

    public void throwFireBall(Character heroDefault) {
        attackWithSpell(heroDefault, fireBall);
    }

    public void castBlizzard(Character heroDefault) {
        attackWithSpell(heroDefault, blizzard);
    }

    public void throwMultiFireBall(ArrayList<Ally> targets) {

        for (Character target : targets) {
            throwFireBall(target);
            // System.out.println("Multi FireBall!");
        }
    }

    /**
     * Generating a random number from lets say 0 to 11
     * if the number is in the:
     * 0 to 10 intervals represent percentages of potential for attack
     * every unit therefore increases the attack change by 10%
     */
    public int aiAttack() {
        switch(random.nextInt(10)) {
            case 0:
            case 1:
            case 2:
                return 0;
            case 3:
            case 4:
            case 5:
                return 1;
            case 6:
            case 7:
                return 2;
            case 8:
            case 9:
                return 3;
        }
        return -1;
    }

    public int resistanceOrWeaknessFilter(OffensiveMagic attackersSpell) {
        Element element = attackersSpell.getElement();
        if (resilience.contains(element)) {
            return resilienceFilter(attackersSpell);
        }
        if (weakness.contains(element)) {
            return weaknessFilter(attackersSpell);
        }
        throw new IllegalStateException();
    }

    /**
     *      Earth type offensive spells apart from Earthquake cannot be currently tested therefore not covered.
     *      That's because there is no other Earth element except from Earthquake but Earthquake does not affect
     *      Dragon cause this type of Monster can fly.
     *      However that would be repeating this same process
     */

    private int resilienceFilter(OffensiveMagic attackersSpell) {
        if (attackersSpell.getElement() == Element.FIRE) {
            return attackersSpell.getDamage()/ WIND_DAMAGE_TAKEN_QUANTIFIER;
        }
        if (attackersSpell.getElement() == Element.EARTH) {
            if (attackersSpell.getSpellType() == OffensiveMagic.SpellType.EARTHQUAKE) {
                return 0;
            }
            return (int) (attackersSpell.getDamage()* EARTH_DAMAGE_TAKEN_QUANTIFIER);
        }
        return (int) (attackersSpell.getDamage()* ICE_DAMAGE_TAKEN_QUANTIFIER);
    }

    private int weaknessFilter(OffensiveMagic attackersSpell) {
        if (attackersSpell.getSpellType() == OffensiveMagic.SpellType.WIND) {
            return attackersSpell.getDamage()*2;
        }
        return TSUNAMI_DAMAGE_TAKEN_QUANTIFIER;
    }
}
