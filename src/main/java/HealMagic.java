public class HealMagic extends Magic {
    private static final String DEFAULT_NAME = "Heal";
    private static final int START_LEVEL = 1;
    private static final int HEAL_AMOUNT = 10;
    private static final int MAX_LEVEL = 5;
    private final String name;
    private int level;
    private int uses;

    public HealMagic() {
        super();
        this.name = DEFAULT_NAME;
        this.level = START_LEVEL;
    }

    public void useHeal(Character target, Character caster) {
        if (level < MAX_LEVEL) {
            uses++;
            levelUpHeal();
        }
        int healAmount = calculateHealAmount();
        target.restoreHP(healAmount);
        caster.useMagic(MPCost);
    }

    public int calculateHealAmount() {
        return HEAL_AMOUNT * level;
    }

    private void levelUpHeal() {
        //low because of tests.
        if (uses % 2 == 0)
            level++;
    }

    public int getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public int getUses() {
        return uses;
    }

}
