public class Priest extends Ally {

    //Magic
    private final HealMagic heal = new HealMagic();

    public Priest(String name, int totalExperience, Equipment equipment, Race race){
        super (name, totalExperience, equipment, race, 2,
                4, 2, 2, 2, 4);
    }

    public void castHeal(Character target){
        if (target == null){
            throw new IllegalArgumentException();
        }
        if(getCurrentMP() >= heal.getMPCost()) {
            heal.useHeal(target, this);
        } else{
            throw new IllegalArgumentException();
        }

    }


}
