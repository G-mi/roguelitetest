public class StatusEffect extends Magic{

    private final static int START_COUNTER = 0;
    private int counter;
    private final int duration;
    private final int statusChange;
    private final Effect effect;
    private final EffectType effectType;

    enum Effect {REGEN(2, 5), POISON(2, 5);
    private final int duration;
    private final int statusChange;

        Effect(int duration, int statusChange ) {
            this.duration = duration;
            this.statusChange = statusChange;
        }

        public int getDuration() {
            return duration;
        }

        public int getStatusChange() {
            return statusChange;
        }
    }

    enum EffectType{HEAL, DAMAGE}

    public StatusEffect( Effect effect, EffectType effectType) {
        this.duration = effect.getDuration();
        this.counter = START_COUNTER;
        this.statusChange = effect.getStatusChange();
        this.effect = effect;
        this.effectType = effectType;
    }

    public void applyEffect(Character target){
        switch (effectType){
            case HEAL:
                target.restoreHP(statusChange);
                increaseCounter();
                break;
            case DAMAGE:
                target.takeDamage(statusChange);
                increaseCounter();
                break;
        }
    }

    private void increaseCounter(){
        counter += 1;    }

    public void resetCounter(){
        counter =0;
    }

    public boolean turnsLeft(){
        return counter < duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatusEffect effect1 = (StatusEffect) o;
        return  duration == effect1.duration &&
                statusChange == effect1.statusChange &&
                effect == effect1.effect &&
                effectType == effect1.effectType;
    }
    /* not used
    @Override
    public int hashCode() {
        return Objects.hash(duration, statusChange, effect, effectType);
    } */

    public int getCounter(){
        return counter;
    }

    public Effect getEffect() {
        return effect;
    }

    public int getDuration() {
        return duration;
    }

    public int getStatusChange() {
        return statusChange;
    }

    public EffectType getEffectType() {
        return effectType;
    }

}
