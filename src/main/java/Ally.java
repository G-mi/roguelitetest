public abstract class Ally extends Character {

    private final int HP_MODIFIER;
    private final int MP_MODIFIER;
    private final int STAMINA_MODIFIER;
    private final int STR_MODIFIER;
    private final int SPD_MODIFIER;
    private final int MAGIC_MODIFIER;

    private final Race race;
    //maxExp = det totala antalet exp som krävs för att nå lv 99
    private final int maxExp = 229271;
    Equipment equipment;
    private int totalExp = 0;
    private int exp = 0;
    private int level = 1;
    private int nextLvExp = getNextLvExp(level + 1);
    private int strength;
    private int magic;
    private int speed;
    private boolean defending = false;


    public Ally(String name, int totalExp, Equipment equipment, Race race, int hpMod, int mpMod,
                int staminaMod, int strMod, int spdMod, int magicMod) {
        super(name, race.baseHp, race.baseMp, race.baseStamina);

        this.HP_MODIFIER = hpMod;
        this.MP_MODIFIER = mpMod;
        this.STAMINA_MODIFIER = staminaMod;
        this.STR_MODIFIER = strMod;
        this.SPD_MODIFIER = spdMod;
        this.MAGIC_MODIFIER = magicMod;

        this.race = race;
        this.strength = race.baseStr;
        this.speed = race.baseSpd;
        this.magic = race.baseMag;

        if(equipment == null){
            throw new IllegalArgumentException();
        }
        else if(equipment.getWeapon() == null || equipment.getArmour() == null){
            throw new IllegalArgumentException();
        }
        else {
            this.equipment = equipment;
        }

        if (totalExp < 0) {
            throw new IllegalArgumentException();
        } else if (totalExp > 0) {
            gainExperience(totalExp);
        }
    }


    public Race getRace() {
        return this.race;
    }

    public int getTotalExperience() {
        return this.totalExp;
    }

    public int getExperience() {
        return this.exp;
    }

    public int getLv() {
        return this.level;
    }


    public void increaseStrength(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        strength = Math.min((strength + amount), 999);
    }

    public void increaseMagic(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        magic = Math.min((magic + amount), 999);
    }

    public void increaseSpeed(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        speed = Math.min((speed + amount), 999);
    }

    public int getStrength() {
        return this.strength;
    }

    public int getMagic() {
        return this.magic;
    }

    public int getSpeed() {
        return this.speed;
    }

    private int getNextLvExp(int lv) {
        return (int) Math.round(Math.pow(lv, 1.9) + 3 * lv);
    }

    public void gainExperience(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        if (level < 99) {
            totalExp += amount;
            exp += amount;
            while (exp >= nextLvExp) {
                exp -= nextLvExp;
                levelUp();
            }
        }
        if (level == 99) {
            totalExp = maxExp;
            exp = 0;
        }
    }

    public int getWeaponDamage() {
        return strength + level + equipment.getWeapon().getDamageRating();
    }

    public void attackWithWeapon(Monster monster) {
        monster.takeDamage(getWeaponDamage());
    }



    public boolean getDefending() {
        return defending;
    }

    public void raiseDefence(boolean raiseDefence) {
        defending = raiseDefence;
    }

    public int getArmourRating() {
        if (getDefending()) {
            return (int) Math.round(1.5 * equipment.getArmour().getArmourRating());
        } else {
            return equipment.getArmour().getArmourRating();
        }
    }

    public Item.Armour getArmour(){
        return equipment.getArmour();
    }

    public Item.Weapon getWeapon(){
        return equipment.getWeapon();
    }

    public void setArmour(Item.Armour armour){
        Item.Armour temp = equipment.getArmour();
        equipment.setArmour(armour);
        Inventory.getInstance().add(new Item(temp));
    }

    public void setWeapon(Item.Weapon weapon){
        Item.Weapon temp = equipment.getWeapon();
        equipment.setWeapon(weapon);
        Inventory.getInstance().add(new Item(temp));
    }

    private void levelUp() {
        level = Math.min((level + 1), 99);
        nextLvExp = getNextLvExp(level + 1);

        increaseMaxHP(HP_MODIFIER);
        increaseMaxMP(MP_MODIFIER);
        increaseMaxStamina(STAMINA_MODIFIER);
        increaseMagic(MAGIC_MODIFIER);
        increaseSpeed(SPD_MODIFIER);
        increaseStrength(STR_MODIFIER);

    }

    public void drinkPotion(Item.Potion potion) {
        if(potion.getRestorableAttribute().equals("HP")) restoreHP(potion.getRestorationAmount());
        else if(potion.getRestorableAttribute().equals("MP")) restoreMagic(potion.getRestorationAmount());
        else restoreStamina(potion.getRestorationAmount());
    }

    public enum Race {
        HUMAN(50, 20, 16, 10, 6, 7),
        ELF(30, 26, 10, 7, 10, 12),
        DWARF(75, 14, 20, 15, 3, 4);

        final int baseHp;
        final int baseMp;
        final int baseStamina;
        final int baseStr;
        final int baseSpd;
        final int baseMag;

        Race(int baseHp, int baseMp, int baseStamina, int baseStr, int baseSpd, int baseMag) {
            this.baseHp = baseHp;
            this.baseMp = baseMp;
            this.baseStamina = baseStamina;
            this.baseStr = baseStr;
            this.baseSpd = baseSpd;
            this.baseMag = baseMag;
        }


    }


    static class Equipment {


        private Item.Armour armour;
        private Item.Weapon weapon;

        Equipment(Item.Armour armour, Item.Weapon weapon) {
            this.armour = armour;
            this.weapon = weapon;
        }

        public Item.Armour getArmour() {
            return armour;
        }

        public void setArmour(Item.Armour armour) {
            this.armour = armour;
        }

        public Item.Weapon getWeapon() {
            return weapon;
        }

        public void setWeapon(Item.Weapon weapon) {
            this.weapon = weapon;
        }
    }

}


