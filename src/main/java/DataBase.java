import java.util.ArrayList;
import java.util.HashMap;

public class DataBase {

    private static final int MAX_MEMBERS = 4;

    private static DataBase dataBaseInstance;

    //Todo needs to be changed to FriendlyCharacter
    private final HashMap<String, Character> partyMembers = new HashMap<>();

    private DataBase() {
    }

    public static DataBase getInstance() {
        if (dataBaseInstance == null) {
            dataBaseInstance = new DataBase();
        }
        return dataBaseInstance;
    }

    //Todo
    public void removeFromList(Character c) {
        if (partyMembers.isEmpty()) throw new IllegalArgumentException();
        partyMembers.remove(c.getName());

    }

    public boolean containsCharacterInMap(Character character) {
        return partyMembers.containsKey(character.getName());
    }

    //Todo change character to friendlyCharacter
    public void addToList(Character... characters) {
        for (Character c :
                characters) {
            if (partyMembers.containsKey(c.getName())) throw new IllegalArgumentException();
            if (partyMembers.size() == MAX_MEMBERS) throw new IllegalStateException();
            partyMembers.put(c.getName(), c);


        }
    }

    public void flush() {
        partyMembers.clear();
    }

    public ArrayList<Character> getPartyMembers() {
        if (partyMembers.isEmpty()) {
            throw new IllegalStateException();
        }
        return new ArrayList<>(partyMembers.values());
    }
}
