import java.util.ArrayList;
import java.util.Iterator;

public abstract class Character {
    private static final int NAME_MAX_LENGTH = 20;

    private final String name;
    private int maxHP;
    private int maxMP;
    private int maxStamina;
    protected int currentHP;
    private int currentMP;
    private int currentStamina;
    private final ArrayList<StatusEffect> statusEffects;

    public Character(String name, int maxHP, int maxMP, int maxStamina) {
        if (name == null || name.isEmpty() || name.length() >= NAME_MAX_LENGTH)
            throw new IllegalArgumentException();
        this.name = name;

        if (maxHP <= 0 || maxMP <= 0 || maxStamina <= 0)
            throw new IllegalArgumentException();
        this.maxHP = maxHP;
        this.maxMP = maxMP;
        this.maxStamina = maxStamina;

        this.currentHP = maxHP;
        this.currentMP = maxMP;
        this.currentStamina = maxStamina;
        statusEffects = new ArrayList<>();
    }

    public void useMagic(int i) {
        currentMP -= i;
    }

    public int getCurrentMP() {
        return currentMP;
    }

    public void restoreMagic(int i) {
        currentMP += i;
        if (currentMP > maxMP) {
            currentMP = maxMP;
        }
    }

    public String getName() {
        return name;
    }

    public void useStamina(int i) {
        currentStamina -= i;
    }

    public int getCurrentStamina() {
        return currentStamina;
    }

    /*
     This method can be overridden when Character is abstract for different Ally and Monster signatures.
     When we gonna have non magic attacks then the signature can change. For now it only casts OffensiveMagic
     */
    public void dealDamage(Character heroDefault, OffensiveMagic magicAttack) {
        if (magicAttack == null) {
            throw new IllegalArgumentException();
        }
        heroDefault.takeDamage(magicAttack.getDamage());
        useMagic(magicAttack.getMPCost());
    }

   public void dealDamage(Character heroDefault, int damage) {
        if (damage < 1) {
            throw new IllegalArgumentException();
        }
        heroDefault.takeDamage(damage);
    }

    public void restoreStamina(int i) {
        currentStamina += i;
        if (currentStamina > maxStamina) {
            currentStamina = maxStamina;
        }
    }

    //Todo Death
    public void takeDamage(int i) {
        currentHP -= i;
        if(currentHP <= 0) currentHP = 0;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void restoreHP(int i) {
        currentHP += i;
        if (currentHP > maxHP) {
            currentHP = maxHP;
        }
    }



    //todo:
    public void addStatusEffect(StatusEffect statusEffect) {
        if (!statusEffects.contains(statusEffect))
            statusEffects.add(statusEffect);
        else throw new IllegalStateException();
    }

    public void applyStatusEffect() {
        Iterator<StatusEffect> iter = statusEffects.iterator();
        while (iter.hasNext()) {
            StatusEffect effect = iter.next();
            if (effect.turnsLeft()) {
                effect.applyEffect(this);
            } else if (!effect.turnsLeft()) { //För test
                iter.remove();
                effect.resetCounter();
            }
        }
    }


    public boolean hasStatusEffect() {
        return !statusEffects.isEmpty();
    }

    public boolean statusEffectsContains(StatusEffect effect) {
        return statusEffects.contains(effect);
    }

    public void increaseMaxHP(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        maxHP = Math.min((maxHP + amount), 9999);
    }

    public int getMaxHp() {
        return maxHP;
    }

    public void increaseMaxMP(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        maxMP = Math.min((maxMP + amount), 999);
    }

    public int getMaxMp() {
        return maxMP;
    }

    public void increaseMaxStamina(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        maxStamina = Math.min((maxStamina + amount), 999);
    }

    public int getMaxStamina() {
        return maxStamina;
    }


}
