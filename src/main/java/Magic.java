public abstract class Magic {

    private static final int DEFAULT_MP_COST = 5;
    int MPCost;

    public Magic(int MPCost) {
        if (MPCost < 0)
            throw new IllegalArgumentException();
        this.MPCost = MPCost;
    }

    public Magic() {
        MPCost = DEFAULT_MP_COST;
    }

    public int getMPCost() {
        return MPCost;
    }


}
