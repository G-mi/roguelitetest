import java.util.ArrayList;
import java.util.Random;

public abstract class

Monster extends Character {

    private final Drop drop;
    protected boolean alive;
    private int dropAmount;

    protected static final int FREEZE_DEFAULT_MANA = 30;

    public Monster(String name, int maxHP, int maxMP, int maxStamina, ArrayList<Item> droppables, int dropAmount) {
        super(name, maxHP, maxMP, maxStamina);
        if (dropAmount > droppables.size()) dropAmount = droppables.size() - 1;
        this.drop = new Monster.Drop(droppables, dropAmount);
        alive = true;
    }

    //Todo 1
    // This need maybe needs to be integrated with the Character method dealOffensiveSpell
    // This thought to be used from the specific offensiveSpell casting method so that all different
    // spells can traffic through this one

    @Override
    public void takeDamage(int i) {

            super.takeDamage(i);
        if(getCurrentHP() <= 0 ){
            Inventory.getInstance().add(getDrop().getDrops());
            this.alive =false;
        }


    }

    public int getDropAmount(){
        return drop.getDropAmount();
    }


    public void receiveOffensiveSpell(OffensiveMagic attackersSpell) {
        if (attackersSpell == null) {
            throw new IllegalArgumentException();
        }
        takeDamage(resistanceOrWeaknessFilter(attackersSpell));
    }


    protected abstract int resistanceOrWeaknessFilter(OffensiveMagic attackersSpell);

    public boolean isAlive() {
        return alive;
    }
    public Drop getDrop(){
        return drop;
    }

    public static class Drop {
        private final int maxDropAmount;
        private final ArrayList<Item> items;

        public Drop(ArrayList<Item> items, int maxDropAmount) {

            this.items = items;
            if (maxDropAmount > items.size()) maxDropAmount = items.size();
            this.maxDropAmount = maxDropAmount;
        }


         ArrayList<Item> getDroppables() {
            return items;
        }


         ArrayList<Item> getDrops() {
            return getDrops(new Random());
        }

        // packageprivate, takes non-random fixer
        ArrayList<Item> getDrops(Random random) {
            //make a random objekt
            ArrayList<Item> drops = new ArrayList<>();
            ArrayList<Item> itemsCopy = new ArrayList<>(items);

            for (int i = 0; i <= random.nextInt(maxDropAmount); i++) {
                int out = random.nextInt(itemsCopy.size());
                drops.add(itemsCopy.get(out));
                itemsCopy.remove(out);
            }
            return drops;
        }

        public int getDropAmount() {
            return maxDropAmount;
        }
    }

}
