public class Item {


    private Armour armour;
    private Weapon weapon;
    private Potion potion;

    public Item(Armour armour) {
        this.armour = armour;
    }

    public Item(Weapon weapon) {
        this.weapon = weapon;
    }

    public Item(Potion potion) {
        this.potion = potion;
    }

    public Item() {

    }


    public Armour getArmour() {
        return armour;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Potion getPotion() {
        return potion;
    }

    public String getType() {
        if (potion != null) return potion.name();
        else if (weapon != null) return weapon.name();
        else if (armour != null) return armour.name();
        return "ERROR";
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Armour armour : Armour.values()) {
            hashCode++;
            if (getType().equals(armour.name())) return hashCode;

        }
        for (Weapon weapon : Weapon.values()) {
            hashCode++;
            if (getType().equals(weapon.name())) return hashCode;

        }
        for (Potion potion : Potion.values()) {
            hashCode++;
            if (getType().equals(potion.name())) return hashCode;

        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) return false;
        Item item = (Item) obj;
        return item.getType().equals(getType());
    }
/*
    @Override
    public String toString() {
        if (armour != null) return armour.name();
        else if (weapon != null) return weapon.name();
        else if (potion != null) return potion.name();
        return "";
    }

 */

    public enum Armour {

        LeatherArmour(5),
        IronArmour(10),
        SteelArmour(20),
        OrichalcumArmour(50);
        private final int armourRating;

        Armour(int armourRating) {
            this.armourRating = armourRating;
        }

        public int getArmourRating() {
            return this.armourRating;
        }
    }

    public enum Potion {

        HealthPotion("HP", 20),
        ManaPotion("MP", 50),
        StaminaPotion("Stamina", 30);
        private final String restorableAttribute;
        private final int restorationAmount;

        Potion(String restorableAttribute, int restorationAmount) {
            this.restorableAttribute = restorableAttribute;
            this.restorationAmount = restorationAmount;
        }

        public String getRestorableAttribute() {
            return restorableAttribute;
        }

        public int getRestorationAmount() {
            return restorationAmount;
        }
    }

    public enum Weapon {
        Sword(20),
        Spear(30),
        Staff(10);
        private final int damageRating;

        Weapon(int damageRating) {
            this.damageRating = damageRating;
        }

        public int getDamageRating() {
            return damageRating;
        }

    }
}
