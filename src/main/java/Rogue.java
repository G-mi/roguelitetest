import java.util.Random;

public class Rogue extends Ally {

    static final int STEAL_STAMINA_COST = 2;

    Random random = new Random();

    public Rogue(String name, int totalExperience, Ally.Equipment equipment, Ally.Race race){
        super (name, totalExperience, equipment, race, 4,
                1, 4, 3, 4, 2);
    }

    public void steal(Monster m, Random random){
        if (getCurrentStamina() >= STEAL_STAMINA_COST) {
            useStamina(STEAL_STAMINA_COST);

            Item stolenItem = m.getDrop().getDroppables().get(
                    random.nextInt(m.getDropAmount()));

            Inventory.getInstance().add(stolenItem);
        }
    }


}
