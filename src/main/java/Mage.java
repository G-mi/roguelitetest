public class Mage extends Ally {

    //Magic
    private final OffensiveMagic fireball = new OffensiveMagic(OffensiveMagic.SpellType.FIREBALL);
    private final OffensiveMagic wind = new OffensiveMagic(OffensiveMagic.SpellType.WIND);
    private final OffensiveMagic tsunami = new OffensiveMagic(OffensiveMagic.SpellType.TSUNAMI);
    private final OffensiveMagic earthquake = new OffensiveMagic(OffensiveMagic.SpellType.EARTHQUAKE);
    private final OffensiveMagic blizzard = new OffensiveMagic(OffensiveMagic.SpellType.BLIZZARD);


    public Mage(String name, int totalExperience, Equipment equipment, Race race){
        super (name, totalExperience, equipment, race, 3,
                5, 1, 1, 1, 5);

    }

    public void castWind(Monster target){
        attackWithSpell(target, wind);
    }

    public void castFireball(Monster target){
        attackWithSpell(target, fireball);
    }

    public void castBlizzard(Monster target){
        attackWithSpell(target, blizzard);
    }

    public void castTsunami(Monster target){
        attackWithSpell(target, tsunami);
    }

    public void castEarthquake(Monster target){
        attackWithSpell(target, earthquake);
    }

    public void attackWithSpell(Monster target, OffensiveMagic spell){
        if(target == null){
            throw new IllegalArgumentException();
        }
        if (getCurrentMP() >= spell.getMPCost()){
            target.receiveOffensiveSpell(spell);
            useMagic(spell.getMPCost());
        }
        else{
            throw new IllegalArgumentException();
        }

    }
}
