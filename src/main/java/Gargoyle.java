/*
Detta klass kommer inte implementeras det är bara för att visa idén att flera typer monsters
kan skapas. Processen blir lika som i klassen Dragon.
 */

import java.util.ArrayList;

public class Gargoyle extends Monster {
    private static final int dropAmount = 1;

    private final OffensiveMagic blizzard;

    private static final ArrayList<Item> droppables = new ArrayList<Item>() {
        {
            add(new Item(Item.Armour.IronArmour));
            add(new Item(Item.Weapon.Staff));
            add(new Item(Item.Potion.ManaPotion));
            add(new Item(Item.Potion.StaminaPotion));
        }
    };


    public Gargoyle(String name, int maxHP, int maxMP, int maxStamina) {
        super(name, maxHP, maxMP, maxStamina, droppables, dropAmount);
        blizzard = new OffensiveMagic(OffensiveMagic.SpellType.BLIZZARD, FREEZE_DEFAULT_MANA);
    }

    @Override
    protected int resistanceOrWeaknessFilter(OffensiveMagic attackersSpell) {
        return 1;
    }
}
