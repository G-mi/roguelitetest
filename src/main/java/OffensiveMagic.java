public class OffensiveMagic extends Magic {

    private final int damage;
    private final Element element;
    private final SpellType spelltype;


    public OffensiveMagic(SpellType spelltype, int MPCost) {
        super(MPCost);
        this.damage = spelltype.getDefaultDamage();
        this.spelltype = spelltype;
        this.element = spelltype.getElement();
    }

    public OffensiveMagic(SpellType spelltype) {
        super();
        this.damage = spelltype.getDefaultDamage();
        this.spelltype = spelltype;
        this.element = spelltype.getElement();
    }

    public int getDamage() {
        return damage;
    }

    public Element getElement() {
        return element;
    }

    public SpellType getSpellType() {
        return spelltype;
    }

    enum SpellType {
        FIREBALL(10, Element.FIRE), WIND(15, Element.WIND), BLIZZARD(5, Element.ICE),
        EARTHQUAKE(10, Element.EARTH), TSUNAMI(15, Element.WATER);

        final int defaultDamage;
        final Element element;

        SpellType(int damage, Element element) {
            this.defaultDamage = damage;
            this.element = element;
        }

        public int getDefaultDamage() {
            return defaultDamage;
        }

        public Element getElement() {
            return element;
        }
    }
}