import java.util.ArrayList;
import java.util.HashMap;

public class Inventory {


    private static final int MAXIMUM_INVENTORY_SIZE = 4;
    private static final Inventory instance = new Inventory();
    private int itemsInInventory = 0;
    private HashMap<Item, Integer> items;
    private final static  int MAX_POTIONS_PER_SLOT_PLUS_ONE = 99;


    private Inventory() {
        reset();
    }

    public static Inventory getInstance() {
        return instance;
    }

    public void reset() {
        this.items = new HashMap<>();
        for (Item.Armour armour : Item.Armour.values()) {
            items.put(new Item(armour), 0);
        }
        for (Item.Weapon weapon :
                Item.Weapon.values()) {
            items.put(new Item(weapon), 0);

        }
        for (Item.Potion potion : Item.Potion.values()
        ) {
            items.put(new Item(potion), 0);
        }
        itemsInInventory = 0;
    }

    public void add(Item item) {
        if (item.getType().equals("ERROR")) throw new IllegalArgumentException();
        if (itemsInInventory == MAXIMUM_INVENTORY_SIZE) throw new IllegalStateException();
        items.put(item, items.get(item) + 1);
        for (Item.Potion potion : Item.Potion.values()) {
            if (potion.name().equals(item.getType())) {
                // potions can be up to 99 for each slot in the inventory
                // tests: checks if you've got 1 element in that slot, or if it's divisible by 99
                if (!(items.get(item) == 1) && !(items.get(item) % MAX_POTIONS_PER_SLOT_PLUS_ONE == 0)) {
                    return;
                }
                break;
            }
        }
        itemsInInventory++;

    }


    public int getItemsInInventoryCount() {
        return itemsInInventory;
    }


    public void remove(Item item) {
        if (items.get(item) == 0) throw new IllegalArgumentException();
        items.put(item, items.get(item) - 1);
        for (Item.Potion potion : Item.Potion.values()
        ) {
            if (item.getType().equals(potion.name())) {
                // potions can be up to 99 for each slot in the inventory
                // tests: checks if you've got 0 elements in that slot, or if it's divisible by 98
                if (!(items.get(item) == 0) || !(items.get(item) % (MAX_POTIONS_PER_SLOT_PLUS_ONE -1) == 0)) return;
                break;
            }
        }
        itemsInInventory--;
    }

    public boolean has(Item item) {
        return items.get(item) > 0;
    }

    public int getItemCount(Item item) {
        return items.get(item);
    }

    public void add(ArrayList<Item> drops) {
        for (Item item : drops
             ) {
            add(item);
        }
    }
}
