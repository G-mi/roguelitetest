import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    private static Inventory inventory;

    @BeforeAll
    static void setUp() {
        inventory = Inventory.getInstance();

    }

    @BeforeEach
    void cleanUp() {
        inventory.reset();
    }


    @Test
    void getInventorySizeTest() {
        inventory.add(new Item(Item.Armour.IronArmour));
        assertEquals(1, inventory.getItemsInInventoryCount());
    }

    @Test
    void getItemListTest() {
        inventory.add(new Item(Item.Potion.StaminaPotion));

        inventory.add(new Item(Item.Armour.OrichalcumArmour));
    }

    @Test
    void addArmourToInventoryTest() {
        inventory.add(new Item(Item.Armour.IronArmour));
        assertTrue(inventory.has(new Item(Item.Armour.IronArmour)));
    }

    @Test
    void addWeaponToInventoryTest() {
        inventory.add(new Item(Item.Weapon.Spear));
        assertTrue(inventory.has(new Item(Item.Weapon.Spear)));
    }

    @Test
    void addPotionToInventoryTest() {
        inventory.add(new Item(Item.Potion.StaminaPotion));
        assertTrue(inventory.has(new Item(Item.Potion.StaminaPotion)));
    }

    @Test
    void addTwoWeaponsToInventoryTest() {
        inventory.add(new Item(Item.Weapon.Staff));
        inventory.add(new Item(Item.Weapon.Spear));
        assertEquals(2, inventory.getItemsInInventoryCount());

    }

    @Test
    void addTwoHealthPotionsTest() {
        inventory.add(new Item(Item.Potion.HealthPotion));
        inventory.add(new Item(Item.Potion.HealthPotion));
        assertEquals(1, inventory.getItemsInInventoryCount());
        assertEquals(2, inventory.getItemCount(new Item(Item.Potion.HealthPotion)));
    }

    @Test
    void removeFromInventoryTest() {
        inventory.add(new Item(Item.Armour.IronArmour));
        inventory.add(new Item(Item.Potion.HealthPotion));
        inventory.add(new Item(Item.Weapon.Spear));
        assertEquals(3, inventory.getItemsInInventoryCount());
        inventory.remove(new Item(Item.Armour.IronArmour));
        inventory.remove(new Item(Item.Weapon.Spear));
        inventory.remove(new Item(Item.Potion.HealthPotion));
        assertEquals(0, inventory.getItemsInInventoryCount());
    }

    @Test
    void potionsStackableTest() {
        int j = 1;
        for (int i = 1; i < 101; i++) {
            inventory.add(new Item(Item.Potion.HealthPotion));
            if (i == 99) j++;
            assertEquals(j, inventory.getItemsInInventoryCount());
        }
    }

    @Test
    void addTooManyItemsTest() {
        for (int i = 0; i < 4; i++) {
            inventory.add(new Item(Item.Armour.IronArmour));
        }
        assertThrows(IllegalStateException.class, () -> inventory.add(new Item(Item.Armour.IronArmour)));
    }

    @Test
    void removeNonexistentArmourTest() {
        assertThrows(IllegalArgumentException.class, () -> inventory.remove(new Item(Item.Armour.LeatherArmour)));

    }

    @Test
    void removeNonexistentWeaponTest() {
        assertThrows(IllegalArgumentException.class, () -> inventory.remove(new Item(Item.Weapon.Spear)));

    }

    @Test
    void removeNonexistentPotionTest() {
        assertThrows(IllegalArgumentException.class, () -> inventory.remove(new Item(Item.Potion.StaminaPotion)));

    }

    @Test
    void removeOnePotionFromStackListTest() {
        int j = 0;
        for (int i = 0; i < 3; i++) {
            assertEquals(j, inventory.getItemsInInventoryCount());
            if (i == 0) j++;
            inventory.add(new Item(Item.Potion.ManaPotion));
        }
        inventory.remove(new Item(Item.Potion.ManaPotion));
        assertEquals(1, inventory.getItemsInInventoryCount());
    }

    @Test
    void nonItemToInventoryTest() {
        assertThrows(IllegalArgumentException.class, () -> inventory.add(new Item()));


    }

    @Test
    void hashCodeAndEqualsforNonItemsTest() {
        Item item = new Item();
        assertEquals(0, item.hashCode());
        Item item2 = new Item();
        assertEquals(item2, item);
    }
}