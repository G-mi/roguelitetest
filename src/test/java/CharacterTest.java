
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CharacterTest {
    private Character defaultCharacter;
    private Character anotherDefaultCharacter;
    private StatusEffect regen;
    private StatusEffect poison;
    private Monster monster;

    @BeforeEach
    void prel() {
        defaultCharacter = new Priest("A", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN);
        regen = new StatusEffect(StatusEffect.Effect.REGEN, StatusEffect.EffectType.HEAL);
        poison = new StatusEffect(StatusEffect.Effect.POISON, StatusEffect.EffectType.DAMAGE);
        monster = new Dragon("dragon", 100, 200, 100);
        anotherDefaultCharacter = new Warrior("B", 0,  new Ally.Equipment(Item.Armour.OrichalcumArmour, Item.Weapon.Spear), Ally.Race.DWARF);
    }


    // CONSTRUCTOR
    @Test
    void constructorStopsTooLongNames() {
        assertThrows(IllegalArgumentException.class, () -> new Priest("Agngiegbipegbnepigigpe", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN));
    }

    @Test
    void emptyNameThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () -> new Priest("", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN));
    }

    @Test
    void nullNameThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () ->

                new Priest(null, 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN));
    }

    @Test
    void negativeHPThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () ->  new Dragon("Test", -50, 80, 100));
    }

    @Test
    void negativeMPThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () -> new Dragon("Test", 50, -80, 100));
    }

    @Test
    void negativeStaminaThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () -> new Dragon("Test", 50, 80, -100));
    }

    @Test
    void zeroHPThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () -> new Dragon("Test", 0,10,100));
    }

    @Test
    void zeroMPThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () -> new Dragon("Test", 50,0,100));
    }

    @Test
    void zeroStaminaThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () -> new Dragon("Test", 50,10,0));
    }

    @Test
    void dieTest() {

        defaultCharacter.takeDamage(50);
        assertEquals(0, defaultCharacter.getCurrentHP());

    }

    @Test
    void nameIsCorrectlySetTest() {
        assertEquals("A", defaultCharacter.getName());
    }

    @Test
    void healingWorks() {
        defaultCharacter.takeDamage(5);
        defaultCharacter.restoreHP(4);
        assertEquals(49, defaultCharacter.getCurrentHP());
    }

    @Test
    void healOverMaxHP() {
        defaultCharacter.takeDamage(5);
        defaultCharacter.restoreHP(10);
        assertEquals(50, defaultCharacter.getCurrentHP());
    }

    @Test
    void takingDamageWorks() {
        defaultCharacter.takeDamage(2);
        assertEquals(48, defaultCharacter.getCurrentHP());
    }


    @Test
    void usingStaminaWorks() {
        defaultCharacter.useStamina(6);
        assertEquals(10, defaultCharacter.getCurrentStamina());
    }

    @Test
    void restoringStaminaWorks() {
        defaultCharacter.useStamina(6);
        defaultCharacter.restoreStamina(5);
        assertEquals(15, defaultCharacter.getCurrentStamina());
    }

    @Test
    void restoreStaminaOverMax() {
        defaultCharacter.useStamina(1);
        defaultCharacter.restoreStamina(5);
        assertEquals(16, defaultCharacter.getCurrentStamina());
    }


    @Test
    void usingMPWorks() {
        defaultCharacter.useMagic(5);
        assertEquals(15, defaultCharacter.getCurrentMP());

    }

    @Test
    void restoringMPWorks() {
        defaultCharacter.useMagic(5);
        defaultCharacter.restoreMagic(3);
        assertEquals(18, defaultCharacter.getCurrentMP());
    }

    @Test
    void restoreMPOverMax() {
        defaultCharacter.useMagic(5);
        defaultCharacter.restoreMagic(10);
        assertEquals(20, defaultCharacter.getCurrentMP());
    }

    @Test
    void dealDamageIAEWZero() {
        assertThrows(IllegalArgumentException.class, () -> defaultCharacter.dealDamage(anotherDefaultCharacter, 0));
    }
    @Test
    void dealDamageIAEWNegative() {
        assertThrows(IllegalArgumentException.class, () -> defaultCharacter.dealDamage(anotherDefaultCharacter, -5));
    }

    @Test
    void dealDamageCorrectAmount() {
        defaultCharacter.dealDamage(anotherDefaultCharacter, 35);
        assertEquals(40, anotherDefaultCharacter.getCurrentHP());
    }

    @Test
    void dealDamageNullTest() {
        assertThrows(IllegalArgumentException.class, () -> defaultCharacter.dealDamage(new Dragon("Test Mon", 10, 10, 10), null));
    }

    @Test
    void addingStatusEffectWorks() {
        defaultCharacter.addStatusEffect(regen);
        assertTrue(defaultCharacter.statusEffectsContains(regen));
    }

    @Test
    void addingSameStatusThrowsIAE() {
        defaultCharacter.addStatusEffect(regen);
        defaultCharacter.applyStatusEffect();
        StatusEffect regen = new StatusEffect(StatusEffect.Effect.REGEN, StatusEffect.EffectType.HEAL);
        assertThrows(IllegalStateException.class, () -> {
            defaultCharacter.addStatusEffect(regen);
        });

    }

    @Test
    void hasStatusEffectCheckWorks() {
        defaultCharacter.addStatusEffect(poison);
        assertTrue(defaultCharacter.hasStatusEffect());
    }

    @Test
    void applyingPoisonWorks() {
        defaultCharacter.addStatusEffect(poison);
        defaultCharacter.applyStatusEffect();
        assertEquals(45, defaultCharacter.getCurrentHP());
        assertEquals(1, poison.getCounter());
    }

    @Test
    void applyingOnMonsterPoisonWorks() {
        monster.addStatusEffect(poison);
        monster.applyStatusEffect();
        assertEquals(95, monster.getCurrentHP());
        assertEquals(1, poison.getCounter());
    }

    @Test
    void applyingPoisonTwoTurnsWorks() {

        defaultCharacter.addStatusEffect(poison);
        defaultCharacter.applyStatusEffect();
        defaultCharacter.applyStatusEffect();
        assertEquals(40, defaultCharacter.getCurrentHP());
        assertEquals(2, poison.getCounter());
    }

    @Test
    void applyingPoisonThreeTurnsWorks() {

        defaultCharacter.addStatusEffect(poison);
        defaultCharacter.applyStatusEffect();
        defaultCharacter.applyStatusEffect();
        defaultCharacter.applyStatusEffect();
        assertEquals(40, defaultCharacter.getCurrentHP());
        assertEquals(0, poison.getCounter());
    }

    @Test
    void applyingTwoEffectsWorks() {

        defaultCharacter.addStatusEffect(poison);
        defaultCharacter.applyStatusEffect();
        defaultCharacter.addStatusEffect(regen);
        defaultCharacter.applyStatusEffect();
        assertEquals(45, defaultCharacter.getCurrentHP());
        assertEquals(2, poison.getCounter());
        assertEquals(1, regen.getCounter());
    }

    @Test
    void applyingTwoEffectsAndRemoveOneWorks() {

        defaultCharacter.addStatusEffect(poison);
        defaultCharacter.applyStatusEffect();
        defaultCharacter.addStatusEffect(regen);
        defaultCharacter.applyStatusEffect();
        defaultCharacter.applyStatusEffect();
        assertEquals(50, defaultCharacter.getCurrentHP());
        assertEquals(0, poison.getCounter());
        assertEquals(2, regen.getCounter());
    }

    @Test
    void applyingRegenWorks() {

        defaultCharacter.addStatusEffect(regen);
        defaultCharacter.takeDamage(10);
        defaultCharacter.applyStatusEffect();
        assertEquals(45, defaultCharacter.getCurrentHP());
        assertEquals(1, regen.getCounter());
    }

    @Test
    void removingStatusEffectWorks() {

        defaultCharacter.addStatusEffect(regen);
        defaultCharacter.applyStatusEffect();
        defaultCharacter.applyStatusEffect();
        defaultCharacter.applyStatusEffect();
        assertFalse(defaultCharacter.hasStatusEffect());
    }
}

