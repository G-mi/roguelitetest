import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MageTest {

    Ally.Equipment equip = new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff);

    @Test
    void mageCanCastWind() {
        Mage m = new Mage("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 100, 10, 10);
        m.castWind(dragon);
        assertEquals(70, dragon.getCurrentHP());
        assertEquals(15, m.getCurrentMP());
    }

    @Test
    void mageCanCastFireball(){
        Mage m = new Mage("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 100, 10, 10);
        m.castFireball(dragon);
        assertEquals(95, dragon.getCurrentHP());
        assertEquals(15, m.getCurrentMP());
    }

    @Test
    void mageCanCastBlizzard(){
        Mage m = new Mage("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 100, 10, 10);
        m.castBlizzard(dragon);
        assertEquals(97, dragon.getCurrentHP());
        assertEquals(15, m.getCurrentMP());
    }

    @Test
    void mageCanCastEarthquake(){
        Mage m = new Mage("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 100, 10, 10);
        m.castEarthquake(dragon);
        assertEquals(100, dragon.getCurrentHP());
        assertEquals(15, m.getCurrentMP());

        //TODO: EarthDamage doesn't seem to be implemented correctly in Dragon
    }

    @Test
    void mageCanCastTsunami(){
        Mage m = new Mage("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 100, 10, 10);
        m.castTsunami(dragon);
        assertEquals(100, dragon.getCurrentHP());
        assertEquals(15, m.getCurrentMP());
    }

    @Test
    void castingOffensiveSpellWithNullAsTargetGivesIAE(){
        Mage m = new Mage("default", 0, equip, Ally.Race.HUMAN);
        assertThrows(IllegalArgumentException.class, () ->
                m.castWind(null));
    }

    @Test
    void castingOffensiveSpellWithNotEnoughMPGivesIAE(){
        Mage m = new Mage("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 100, 10, 10);
        m.useMagic(m.getCurrentMP());
        assertThrows(IllegalArgumentException.class, ()->
                m.castWind(dragon));
    }

}
