import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RogueTest {

    Ally.Equipment equip = new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff);
    //this needs to run inventory.flush before each test
    Inventory inventory =  Inventory.getInstance();

    @BeforeEach
    void prel(){
        inventory.reset();
    }

    @Test
    void stealAddsRandomDroppableItemFromMonsterInInventory(){
        Rogue r = new Rogue("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 21, 10, 10);
        r.steal(dragon, new Random(){
            @Override
            public int nextInt(int i){
                return 0;
            }
        });

        assertTrue(inventory.has(dragon.getDrop().getDroppables().get(0)));
    }

    @Test
    void stealCostsCorrectAmountOfStamina(){
        Rogue r = new Rogue("default", 0, equip, Ally.Race.HUMAN);
        Dragon dragon = new Dragon("default", 21, 10, 10);
        r.steal(dragon, new Random(){
            @Override
            public int nextInt(int i){
                return 0;
            }
        });
        assertEquals(14, r.getCurrentStamina());
    }

    @Test
    void stealDoesNotStealIfNotEnoughStamina(){
        Rogue r = new Rogue("default", 0, equip, Ally.Race.HUMAN);
        r.useStamina(20);
        Dragon dragon = new Dragon("default", 21, 10, 10);
        r.steal(dragon, new Random(){
            @Override
            public int nextInt(int i){
                return 0;
            }
        });
        assertFalse(inventory.has(dragon.getDrop().getDroppables().get(0)));
    }

    @Test
    void stealDoesNotStealIfMonsterDoesNotHaveItem(){
        //Ett test som vi tänkte på, men eftersom Monster alltid kommer att ha items så
        //implementerade vi inte det
    }



}
