import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PriestTest {

    Ally.Equipment equip = new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff);

    @Test
    void castHealHealsCorrectAmount(){
        Priest p = new Priest("default", 0, equip, Ally.Race.HUMAN);
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        w.takeDamage(49);
        p.castHeal(w);
        assertEquals(11, w.getCurrentHP());
        assertEquals(15, p.getCurrentMP());
    }

    @Test
    void castHealWhileNotEnoughMPGivesIAE(){
        Priest p = new Priest("default", 0, equip, Ally.Race.HUMAN);
        p.useMagic(20);
        assertThrows(IllegalArgumentException.class, ()->
                p.castHeal(p));

    }

    @Test
    void castingHealOnTargetNullGivesIAE(){
        Priest p = new Priest("default", 0, equip, Ally.Race.HUMAN);
        assertThrows(IllegalArgumentException.class, () ->
                p.castHeal(null));
    }
}
