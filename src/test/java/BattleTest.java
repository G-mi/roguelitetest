import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {
    ArrayList<Ally> allies;
    Monster monster;
    Battle battle;
    private StatusEffect regen;
    private StatusEffect poison;

    @BeforeEach
    void beforeEach() {
        allies = new ArrayList<Ally>() {
            {
                add(new Priest("HumanPriest", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN));
                add(new Warrior("DwarfWarrior", 0,  new Ally.Equipment(Item.Armour.OrichalcumArmour, Item.Weapon.Spear), Ally.Race.DWARF));
                add(new Mage("ElfMage", 0,  new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff), Ally.Race.ELF));
                add(new Rogue("ElfRogue", 0,  new Ally.Equipment(Item.Armour.SteelArmour, Item.Weapon.Sword), Ally.Race.ELF));
            }
        };
        monster = new Dragon("dragon", 100, 200, 100);
        battle = new Battle(monster, allies);
        regen = new StatusEffect(StatusEffect.Effect.REGEN, StatusEffect.EffectType.HEAL);
        poison = new StatusEffect(StatusEffect.Effect.POISON, StatusEffect.EffectType.DAMAGE);
    }

    @Test
    void getBattlersTest() {
        assertEquals(allies, battle.getBattlers());
    }

    @Test
    void getMonsterTest() {
        assertEquals(monster, battle.getMonster());
    }

    @Test
    void allyUseItemTest() {
        battle.allyTakeDamage(battle.getBattlers().get(0), 21);
        assertEquals(29, battle.getBattlers().get(0).getCurrentHP());
        battle.usePotion(battle.getBattlers().get(0), Item.Potion.HealthPotion);
        assertEquals(49, battle.getBattlers().get(0).getCurrentHP());
    }

    @Test
    void oneAllyDiesTest() {
        battle.allyTakeDamage(battle.getBattlers().get(1), 9999999 );
        allies.remove(1);
        assertEquals(allies, battle.getBattlers());
    }

    @Test
    void finalAllyDiesTest() {
        for(int i = 0; i < 4; i++){
            battle.allyTakeDamage(battle.getBattlers().get(0), 9999999 );
        }
        assertEquals(new ArrayList<Ally>(), battle.getBattlers());
    }

    @Test
    void monsterDiesTest() {
        Inventory.getInstance().reset();
        monster.takeDamage(99999);
        assertNotEquals(0, Inventory.getInstance().getItemsInInventoryCount());
    }

    @Test
    void monsterSingleTargetAttackTest() {
        battle.monsterAttack(0);
        assertEquals(battle.getBattlers().get(0).getMaxHp() - 10, battle.getBattlers().get(0).getCurrentHP());
        battle.monsterAttack(1);
        assertEquals(battle.getBattlers().get(0).getMaxHp() - 15, battle.getBattlers().get(0).getCurrentHP());
    }

    @Test
    void monsterMultiTargetAttackTest() {
        battle.monsterAttack(2);
        for (Character target : battle.getBattlers()) {
            assertEquals(target.getMaxHp() - OffensiveMagic.SpellType.FIREBALL.defaultDamage, target.getCurrentHP());
        }
    }

    @Test
    void monsterAttackOutsideOfPossibilityTest() {
        assertThrows(IllegalArgumentException.class, () -> battle.monsterAttack(4));
    }

    @Test
    void allyActionsTest(){
        //melee
        battle.battleActions(battle.getBattlers().get(0), 0);
        assertEquals(battle.getMonster().getMaxHp() - 21, battle.getMonster().getCurrentHP());


        //class action
        battle.battleActions(battle.getBattlers().get(1), 1);
        Warrior w = (Warrior) battle.getBattlers().get(1);
        assertTrue(w.isCountering());

        //defend
        battle.battleActions(battle.getBattlers().get(0), 2);
        assertEquals(allies.get(0).getArmourRating(), battle.getBattlers().get(0).getArmourRating());


        //potions
        battle.getBattlers().get(0).takeDamage(21);
        battle.battleActions(battle.getBattlers().get(0), 3);
        assertEquals(49,battle.getBattlers().get(0).getCurrentHP());
    }

    @Test
    void priestClassActionTest(){

        battle.getBattlers().get(0).takeDamage(20);
        battle.battleActions(battle.getBattlers().get(0), 1);
        assertEquals(40, battle.getBattlers().get(0).getCurrentHP());
    }

    @Test
    void rogueClassActionTest(){
        Inventory.getInstance().reset();
        battle.battleActions(battle.getBattlers().get(3), 1);
        assertEquals(Inventory.getInstance().getItemsInInventoryCount(), 1);
    }

    @Test
    void mageClassActionTest(){
        battle.battleActions(battle.getBattlers().get(2), 1);
        assertEquals(97, battle.getMonster().getCurrentHP());
    }

    @Test
    void MonsterWinsTest(){
        Scanner scanner = new Scanner("2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2  ");
        assertEquals(0, battle.battle(scanner));
    }

    @Test
    void AlliesWinTest(){
        Scanner scanner = new Scanner("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
        assertEquals(1, battle.battle(scanner));
    }

    @Test
    void applyingPoisonTest(){
        monster.addStatusEffect(poison);
        Scanner scanner = new Scanner("2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2  ");
        battle.battle(scanner);
        assertEquals(90, monster.getCurrentHP());
        assertEquals(0,poison.getCounter());
    }

    @Test
    void AlliesWinTestWithStatusEffect(){
        battle.getBattlers().get(3).addStatusEffect(regen);
        Scanner scanner = new Scanner("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
        assertEquals(1, battle.battle(scanner));
    }

}