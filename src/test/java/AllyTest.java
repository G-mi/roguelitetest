import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AllyTest {

    Ally.Equipment equip = new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff);

    private final int maxExp = 229271;


    @BeforeEach
    void prel(){
        Inventory.getInstance().reset();
    }

    @Test
    void gainExpNotResultInLvUp() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(1, d.getLv());
        assertEquals(0, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        d.gainExperience(9);
        assertEquals(9, d.getTotalExperience());
        assertEquals(9, d.getExperience());
        assertEquals(1, d.getLv());

    }

    @Test
    void gainExperienceResultInOneLvUp() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(1, d.getLv());
        assertEquals(0, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        d.gainExperience(10);
        assertEquals(10, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        assertEquals(2, d.getLv());
    }

    @Test
    void gainExperienceResultInTwoLvUp() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(1, d.getLv());
        assertEquals(0, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        d.gainExperience(27);
        assertEquals(27, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        assertEquals(3, d.getLv());
    }

    @Test
    void gainZeroExperience() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(1, d.getLv());
        assertEquals(0, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        assertThrows(IllegalArgumentException.class, () -> d.gainExperience(0));
    }

    @Test
    void gainNegativeExperience() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(1, d.getLv());
        assertEquals(0, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        assertThrows(IllegalArgumentException.class, () -> d.gainExperience(-1));
    }

    @Test
    void gainExperienceAtMaxLv() {
        Warrior d = new Warrior("default", maxExp, equip, Ally.Race.HUMAN);
        assertEquals(99, d.getLv());
        assertEquals(maxExp, d.getTotalExperience());
        assertEquals(0, d.getExperience());
        d.gainExperience(6610);
        assertEquals(99, d.getLv());
        assertEquals(maxExp, d.getTotalExperience());
        assertEquals(0, d.getExperience());
    }

    @Test
    void levelingUpToMaxLvWithMoreExperienceThanEnough() {
        Warrior d = new Warrior("default", (maxExp-1), equip, Ally.Race.HUMAN);
        assertEquals(98, d.getLv());
        assertEquals((maxExp-1), d.getTotalExperience());
        assertEquals(6486, d.getExperience());
        d.gainExperience(2);
        assertEquals(99, d.getLv());
        assertEquals(maxExp, d.getTotalExperience());
        assertEquals(0, d.getExperience());
    }

    @Test
    void warriorGetsCorrectStatsOnLevelUp() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(50, d.getMaxHp());
        assertEquals(20, d.getMaxMp());
        assertEquals(16, d.getMaxStamina());
        assertEquals(10, d.getStrength());
        assertEquals(6, d.getSpeed());
        assertEquals(7, d.getMagic());
        d.gainExperience(10);
        assertEquals(56, d.getMaxHp());
        assertEquals(22, d.getMaxMp());
        assertEquals(21, d.getMaxStamina());
        assertEquals(14, d.getStrength());
        assertEquals(7, d.getSpeed());
        assertEquals(8, d.getMagic());
    }

    @Test
    void rogueGetsCorrectStatsOnLevelUp() {
        Rogue d = new Rogue("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(50, d.getMaxHp());
        assertEquals(20, d.getMaxMp());
        assertEquals(16, d.getMaxStamina());
        assertEquals(10, d.getStrength());
        assertEquals(6, d.getSpeed());
        assertEquals(7, d.getMagic());
        d.gainExperience(10);
        assertEquals(54, d.getMaxHp());
        assertEquals(21, d.getMaxMp());
        assertEquals(20, d.getMaxStamina());
        assertEquals(13, d.getStrength());
        assertEquals(10, d.getSpeed());
        assertEquals(9, d.getMagic());
    }

    @Test
    void mageGetsCorrectStatsOnLevelUp() {
        Mage d = new Mage("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(50, d.getMaxHp());
        assertEquals(20, d.getMaxMp());
        assertEquals(16, d.getMaxStamina());
        assertEquals(10, d.getStrength());
        assertEquals(6, d.getSpeed());
        assertEquals(7, d.getMagic());
        d.gainExperience(10);
        assertEquals(53, d.getMaxHp());
        assertEquals(25, d.getMaxMp());
        assertEquals(17, d.getMaxStamina());
        assertEquals(11, d.getStrength());
        assertEquals(7, d.getSpeed());
        assertEquals(12, d.getMagic());
    }

    @Test
    void priestGetsCorrectStatsOnLevelUp() {
        Priest d = new Priest("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(50, d.getMaxHp());
        assertEquals(20, d.getMaxMp());
        assertEquals(16, d.getMaxStamina());
        assertEquals(10, d.getStrength());
        assertEquals(6, d.getSpeed());
        assertEquals(7, d.getMagic());
        d.gainExperience(10);
        assertEquals(52, d.getMaxHp());
        assertEquals(24, d.getMaxMp());
        assertEquals(18, d.getMaxStamina());
        assertEquals(12, d.getStrength());
        assertEquals(8, d.getSpeed());
        assertEquals(11, d.getMagic());
    }

    @Test
    void statsDoNotGoAboveLimit() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        d.increaseMaxStamina(999);
        assertEquals(999, d.getMaxStamina());
        d.increaseMaxHP(9999);
        assertEquals(9999, d.getMaxHp());
        d.increaseMaxMP(999);
        assertEquals(999, d.getMaxMp());
        d.increaseStrength(999);
        assertEquals(999, d.getStrength());
        d.increaseSpeed(999);
        assertEquals(999, d.getSpeed());
        d.increaseMagic(999);
        assertEquals(999, d.getMagic());
    }

    @Test
    void addingNegativeStatsGivesIAE() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertThrows(IllegalArgumentException.class, () -> d.increaseMaxStamina(-1));
        assertThrows(IllegalArgumentException.class, () -> d.increaseMaxHP(-1));
        assertThrows(IllegalArgumentException.class, () -> d.increaseMaxMP(-1));
        assertThrows(IllegalArgumentException.class, () -> d.increaseStrength(-1));
        assertThrows(IllegalArgumentException.class, () -> d.increaseSpeed(-1));
        assertThrows(IllegalArgumentException.class, () -> d.increaseMagic(-1));
    }

    @Test
    void addingNegativeExpInConstructorGivesIAE(){
        assertThrows(IllegalArgumentException.class, () ->
        new Warrior("default", -1, equip, Ally.Race.HUMAN));
    }

    @Test
    void addingMoreThanMaxExpInConstructorMakesTotalExpMaxExp(){
        Warrior d = new Warrior("default", (maxExp+1), equip, Ally.Race.HUMAN);
        assertEquals(maxExp, d.getTotalExperience());
    }

    @Test
    void addingPositiveExperienceInConstructorResultsInCorrectLevel(){
        Warrior d = new Warrior("default", 10, equip, Ally.Race.HUMAN);
        assertEquals(0, d.getExperience());
        assertEquals(10, d.getTotalExperience());
        assertEquals(2, d.getLv());
    }

    @Test
    void addingZeroToStatsGivesIAE() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertThrows(IllegalArgumentException.class, () -> d.increaseMaxStamina(0));
        assertThrows(IllegalArgumentException.class, () -> d.increaseMaxHP(0));
        assertThrows(IllegalArgumentException.class, () -> d.increaseMaxMP(0));
        assertThrows(IllegalArgumentException.class, () -> d.increaseStrength(0));
        assertThrows(IllegalArgumentException.class, () -> d.increaseSpeed(0));
        assertThrows(IllegalArgumentException.class, () -> d.increaseMagic(0));
    }

    @Test
    void baseStatsOfHumanCorrect() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(50, d.getMaxHp());
        assertEquals(20, d.getMaxMp());
        assertEquals(16, d.getMaxStamina());
        assertEquals(10, d.getStrength());
        assertEquals(6, d.getSpeed());
        assertEquals(7, d.getMagic());
    }

    @Test
    void baseStatsOfElfCorrect() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.ELF);
        assertEquals(30, d.getMaxHp());
        assertEquals(26, d.getMaxMp());
        assertEquals(10, d.getMaxStamina());
        assertEquals(7, d.getStrength());
        assertEquals(10, d.getSpeed());
        assertEquals(12, d.getMagic());

    }

    @Test
    void baseStatsOfDwarfCorrect() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.DWARF);
        assertEquals(75, d.getMaxHp());
        assertEquals(14, d.getMaxMp());
        assertEquals(20, d.getMaxStamina());
        assertEquals(15, d.getStrength());
        assertEquals(3, d.getSpeed());
        assertEquals(4, d.getMagic());
    }

    @Test
    void attackingWithWeaponDealsDamageToMonster() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        Dragon m = new Dragon("default", 100, 10, 10);
        d.attackWithWeapon(m);
        assertEquals(79, m.getCurrentHP());
    }

    @Test
    void attackingWithWeaponKillsMonster() {

        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        Dragon m = new Dragon("default", 21, 10, 10);
        d.attackWithWeapon(m);
        assertEquals(0, m.getCurrentHP());
        assertFalse(m.isAlive());
    }

    @Test
    void defendingFalseByDefault() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertFalse(d.getDefending());
    }

    @Test
    void raiseDefenceMakesDefendingTrue() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        d.raiseDefence(true);
        assertTrue(d.getDefending());
    }

    @Test
    void raiseDefenceMakesDefendingFalse() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        d.raiseDefence(true);
        d.raiseDefence(false);
        assertFalse(d.getDefending());
    }

    @Test
    void armourRatingCorrectNormallyWithArmour() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(10, d.getArmourRating());
    }

    @Test
    void armourRatingCorrectWhileDefendingWithArmour() {
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        d.raiseDefence(true);
        assertEquals(15, d.getArmourRating());
    }

    @Test
    void settingEquipmentAsNullThrowsIAE(){
        assertThrows(IllegalArgumentException.class, () ->
                new Warrior("default", 0, null, Ally.Race.HUMAN));
    }

    @Test
    void settingWeaponAsNullThrowsIAE(){
        Ally.Equipment noWeapon = new Ally.Equipment(Item.Armour.IronArmour, null);
        assertThrows(IllegalArgumentException.class, () ->
                new Warrior("default", 0, noWeapon, Ally.Race.HUMAN));
    }

    @Test
    void settingArmourAsNullThrowsIAE(){
        Ally.Equipment noArmour = new Ally.Equipment(null, Item.Weapon.Staff);
        assertThrows(IllegalArgumentException.class, () ->
                new Warrior("default", 0, noArmour, Ally.Race.HUMAN));
    }

    @Test
    void getEquipmentWorks(){
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(equip.getWeapon(), d.getWeapon());
        assertEquals(equip.getArmour(), d.getArmour());
    }

    @Test
    void setEquipment(){
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        Item.Weapon weapon = Item.Weapon.Spear;
        Item.Armour armour = Item.Armour.LeatherArmour;
        d.setWeapon(weapon);
        d.setArmour(armour);
        assertEquals(weapon, d.getWeapon());
        assertEquals(armour, d.getArmour());
    }

    @Test
    void getRaceGivesCorrectRace(){
        Warrior d = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertEquals(Ally.Race.HUMAN, d.getRace());
    }

    @Test
    void hpPotionHealsCorrectAmount(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        w.takeDamage(49);
        Item.Potion hpPotion = Item.Potion.HealthPotion;
        w.drinkPotion(hpPotion);
        assertEquals(21, w.getCurrentHP());
    }

    @Test
    void mpPotionRestoresCorrectAmount(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        Item.Potion mpPotion = Item.Potion.ManaPotion;
        w.useMagic(20);
        w.increaseMaxMP(40);
        w.drinkPotion(mpPotion);
        assertEquals(50, w.getCurrentMP());
    }

    @Test
    void staminaPotionRestoresCorrectAmount(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        Item.Potion staminaPotion = Item.Potion.StaminaPotion;
        w.useStamina(16);
        w.increaseMaxStamina(15);
        w.drinkPotion(staminaPotion);
        assertEquals(30, w.getCurrentStamina());
    }

    //Ekvivalensklass test för konstruktorn

    @Test
    void ekvTest1(){
        Warrior w = new Warrior("A", 0, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                Ally.Race.HUMAN);
        assertEquals("A", w.getName());
        assertEquals(0, w.getTotalExperience());
        assertEquals(Ally.Race.HUMAN, w.getRace());
        assertEquals(Item.Armour.IronArmour, w.getArmour());
        assertEquals(Item.Weapon.Staff, w.getWeapon());
    }

    @Test
    void ekvTest2(){
        Rogue r = new Rogue("Aa", 1, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                Ally.Race.ELF);
        assertEquals("Aa", r.getName());
        assertEquals(1, r.getTotalExperience());
        assertEquals(Ally.Race.ELF, r.getRace());
        assertEquals(Item.Armour.IronArmour, r.getArmour());
        assertEquals(Item.Weapon.Staff, r.getWeapon());
    }

    @Test
    void ekvTest3(){
        //name has 19 characters
        Priest p = new Priest("Aaaaaaaaaaaaaaaaaaa", 2, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                Ally.Race.DWARF);
        assertEquals("Aaaaaaaaaaaaaaaaaaa", p.getName());
        assertEquals(2, p.getTotalExperience());
        assertEquals(Ally.Race.DWARF, p.getRace());
        assertEquals(Item.Armour.IronArmour, p.getArmour());
        assertEquals(Item.Weapon.Staff, p.getWeapon());
    }

    @Test
    void ekvTest4(){
        Mage m = new Mage("A", maxExp, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                Ally.Race.HUMAN);
        assertEquals("A", m.getName());
        assertEquals(maxExp, m.getTotalExperience());
        assertEquals(Ally.Race.HUMAN, m.getRace());
        assertEquals(Item.Armour.IronArmour, m.getArmour());
        assertEquals(Item.Weapon.Staff, m.getWeapon());
    }

    @Test
    void ekvTest5(){
        assertThrows(IllegalArgumentException.class, () ->
                new Warrior(null, 0, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                        Ally.Race.HUMAN));
    }

    @Test
    void ekvTest6(){
        assertThrows(IllegalArgumentException.class, () ->
                new Warrior("", 0, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                        Ally.Race.HUMAN));
    }

    @Test
    void ekvTest7(){
        assertThrows(IllegalArgumentException.class, () ->
                new Warrior("AaaaaaaaaaaaaaaaaaaA", 0, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                        Ally.Race.HUMAN));
    }

    @Test
    void ekvTest8(){
        assertThrows(IllegalArgumentException.class, () ->
                new Warrior("A", -1, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                        Ally.Race.HUMAN));
    }

    @Test
    void ekvTest9(){
        Warrior w = new Warrior("A", maxExp+1, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                Ally.Race.HUMAN);
        assertEquals("A", w.getName());
        assertEquals(maxExp, w.getTotalExperience());
        assertEquals(Ally.Race.HUMAN, w.getRace());
        assertEquals(Item.Armour.IronArmour, w.getArmour());
        assertEquals(Item.Weapon.Staff, w.getWeapon());
    }

    @Test
    void ekvTest10(){
        assertThrows(IllegalArgumentException.class, ()->
                new Warrior("A", 0, new Ally.Equipment(Item.Armour.IronArmour, null),
                        Ally.Race.HUMAN));
    }

    @Test
    void ekvTest11(){
        assertThrows(IllegalArgumentException.class, ()->
                new Warrior("A", 0, new Ally.Equipment(null, Item.Weapon.Staff),
                        Ally.Race.HUMAN));

    }

    @Test
    void ekvTest12(){
        assertThrows(NullPointerException.class, ()->
                new Warrior("A", 0, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff),
                        null));
    }

    @Test
    void ekvTest13(){
        assertThrows(IllegalArgumentException.class, ()->
                new Warrior("A", 0, null,
                        Ally.Race.HUMAN));

    }

    //todo remove for integration tests
    static class EquipmentTest {
        Item.Armour armour = Item.Armour.OrichalcumArmour;
        Item.Weapon weapon = Item.Weapon.Spear;
        Ally.Equipment equipment;

        @BeforeEach
        void prel() {
            equipment = new Ally.Equipment(armour, weapon);
        }

        @Test
        void getArmourTest() {
            assertEquals(Item.Armour.OrichalcumArmour, equipment.getArmour());
        }

        @Test
        void setArmourTest() {
            equipment.setArmour(Item.Armour.IronArmour);
            assertEquals(Item.Armour.IronArmour, equipment.getArmour());
        }

        @Test
        void getWeaponTest() {
            assertEquals(Item.Weapon.Spear, equipment.getWeapon());
        }

        @Test
        void setWeaponTest() {
            equipment.setWeapon(Item.Weapon.Staff);
            assertEquals(Item.Weapon.Staff, equipment.getWeapon());
        }

    }
}
