import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DataBaseTest {

    DataBase defaultDataBase;

    //Todo add FriendlyCharacter
    Ally defaultCharacter;

    @BeforeEach
    void prel() {
        defaultDataBase = DataBase.getInstance();
        defaultCharacter = new Priest("Test", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN);
    }

    @AfterEach
    void done() {
        defaultDataBase.flush();
    }

    @Test
    void defaultDataBaseNotNull() {
        assertNotNull(defaultDataBase);
    }

    @Test
    void addCharacterToMapTest() {
        defaultDataBase.addToList(defaultCharacter);
        assertTrue(defaultDataBase.containsCharacterInMap(defaultCharacter));
    }

    @Test
    void removeCharacterFromMapTest() {
        defaultDataBase.addToList(defaultCharacter);
        defaultDataBase.removeFromList(defaultCharacter);
        assertFalse(defaultDataBase.containsCharacterInMap(defaultCharacter));
    }

    @Test
    void addingDuplicateCharacter() {
        defaultDataBase.addToList(defaultCharacter);
        assertThrows(IllegalArgumentException.class, () -> defaultDataBase.addToList(defaultCharacter));
    }

    @Test
    void maximumCapacityTest() {
        defaultDataBase.addToList(new Priest("HumanPriest", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN),
                                  new Warrior("DwarfWarrior", 0,  new Ally.Equipment(Item.Armour.OrichalcumArmour, Item.Weapon.Spear), Ally.Race.DWARF),
                                  new Mage("ElfMage", 0,  new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff), Ally.Race.ELF),
                                  new Rogue("ElfRogue", 0,  new Ally.Equipment(Item.Armour.SteelArmour, Item.Weapon.Sword), Ally.Race.ELF));
        assertThrows(IllegalStateException.class, () -> defaultDataBase.addToList(defaultCharacter));
    }

    // Might end up being implemented differently
    @Test
    void removingCharacterFromEmptyMap() {
        assertThrows(IllegalArgumentException.class, () -> defaultDataBase.removeFromList(defaultCharacter));
    }

    @Test
    void getMembersLengthTest() {
        defaultDataBase.addToList(
                new Warrior("DwarfWarrior", 0,  new Ally.Equipment(Item.Armour.OrichalcumArmour, Item.Weapon.Spear), Ally.Race.DWARF),
                new Mage("ElfMage", 0,  new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff), Ally.Race.ELF),
                new Rogue("ElfRogue", 0,  new Ally.Equipment(Item.Armour.SteelArmour, Item.Weapon.Sword), Ally.Race.ELF));
        ArrayList<Character> membersList = defaultDataBase.getPartyMembers();
        assertEquals(3, membersList.size());
    }

    @Test
    void getMembersTest() {
        defaultDataBase.addToList(
                new Priest("A", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN),
                new Warrior("B", 0,  new Ally.Equipment(Item.Armour.OrichalcumArmour, Item.Weapon.Spear), Ally.Race.DWARF),
                new Mage("C", 0,  new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff), Ally.Race.ELF),
                new Rogue("D", 0,  new Ally.Equipment(Item.Armour.SteelArmour, Item.Weapon.Sword), Ally.Race.ELF));
        ArrayList<Character> membersList = defaultDataBase.getPartyMembers();
        assertEquals("A", membersList.get(0).getName());
        assertEquals("B", membersList.get(1).getName());
        assertEquals("C", membersList.get(2).getName());
        assertEquals("D", membersList.get(3).getName());
    }

    @Test
    void getEmptyMembersThrowsIAE() {
        assertThrows(IllegalStateException.class, () -> defaultDataBase.getPartyMembers());
    }
}
