import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class WarriorTest {

    Ally.Equipment equip = new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff);

    @Test
    void isCounteringFalseByDefault(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        assertFalse(w.isCountering());
    }

    @Test
    void counterTrueMakesCounteringTrue(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        w.setCountering(true);
        assertTrue(w.isCountering());
    }

    @Test
    void counterCostsCorrectAmountOfStamina(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        w.setCountering(true);
        w.takeDamage(2,new Dragon("d",100 , 50, 50));
        assertEquals(12, w.getCurrentStamina());
    }

    @Test
    void counteringWithNotEnoughStaminaDoesNothing(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        w.useStamina(20);
        Dragon d = new Dragon("d",100 , 50, 50);
        w.takeDamage(3,d);
        assertEquals(100, d.getCurrentHP());

    }

    @Test
    void attackedWhileCounteringAttacksEnemy(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        Dragon d = new Dragon("d",100 , 50, 50);
        w.setCountering(true);
        w.takeDamage(3,d);
        assertEquals(79, d.getCurrentHP());
    }

    @Test
    void attackedWhileCounteringDealsZeroDamage(){
        Warrior w = new Warrior("default", 0, equip, Ally.Race.HUMAN);
        Dragon d = new Dragon("d",100 , 50, 50);
        w.setCountering(true);
        w.takeDamage(4,d);
        assertEquals(50, w.getCurrentHP());
    }

}
