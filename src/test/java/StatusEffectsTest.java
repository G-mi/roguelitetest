import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StatusEffectsTest {
    

    @Test
    void healConstructorWorks() {
        StatusEffect regen = new StatusEffect(StatusEffect.Effect.REGEN, StatusEffect.EffectType.HEAL);
        assertEquals(2, regen.getDuration());
        assertEquals(0, regen.getCounter());
        assertEquals(5, regen.getStatusChange());
        assertEquals(5, regen.getMPCost());
        assertEquals(StatusEffect.Effect.REGEN, regen.getEffect());
        assertEquals(StatusEffect.EffectType.HEAL, regen.getEffectType());
    }

    @Test
    void poisonConstructorWorks() {
        StatusEffect poison= new StatusEffect(StatusEffect.Effect.POISON, StatusEffect.EffectType.DAMAGE);
        assertEquals(2, poison.getDuration());
        assertEquals(0, poison.getCounter());
        assertEquals(5, poison.getStatusChange());
        assertEquals(5, poison.getMPCost());
        assertEquals(StatusEffect.Effect.POISON, poison.getEffect());
        assertEquals(StatusEffect.EffectType.DAMAGE, poison.getEffectType());
    }
}

    
