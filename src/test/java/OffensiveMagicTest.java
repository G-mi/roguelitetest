import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class OffensiveMagicTest {

    @Test
    void fireballMagicDefaultMPConstructorWorks() {
        OffensiveMagic fireball = new OffensiveMagic(OffensiveMagic.SpellType.FIREBALL);
        assertEquals(10, fireball.getDamage());
        assertEquals(OffensiveMagic.SpellType.FIREBALL, fireball.getSpellType());
        assertEquals(Element.FIRE, fireball.getElement());
        assertEquals(5, fireball.getMPCost());
    }

    @Test
    void windMagicDefaultMPConstructorWorks() {
        OffensiveMagic wind = new OffensiveMagic(OffensiveMagic.SpellType.WIND);
        assertEquals(15, wind.getDamage());
        assertEquals(OffensiveMagic.SpellType.WIND, wind.getSpellType());
        assertEquals(Element.WIND, wind.getElement());
        assertEquals(5, wind.getMPCost());
    }

    @Test
    void freezeMagicDefaultMPConstructorWorks() {
        OffensiveMagic freeze = new OffensiveMagic(OffensiveMagic.SpellType.BLIZZARD);
        assertEquals(5, freeze.getDamage());
        assertEquals(OffensiveMagic.SpellType.BLIZZARD, freeze.getSpellType());
        assertEquals(Element.ICE, freeze.getElement());
        assertEquals(5, freeze.getMPCost());
    }

    @Test
    void earthquakeMagicDefaultMPConstructorWorks() {
        OffensiveMagic earthquake = new OffensiveMagic(OffensiveMagic.SpellType.EARTHQUAKE);
        assertEquals(10, earthquake.getDamage());
        assertEquals(OffensiveMagic.SpellType.EARTHQUAKE, earthquake.getSpellType());
        assertEquals(Element.EARTH, earthquake.getElement());
        assertEquals(5, earthquake.getMPCost());
    }

    @Test
    void tsunamiMagicDefaultMPConstructorWorks() {
        OffensiveMagic tsunami = new OffensiveMagic(OffensiveMagic.SpellType.TSUNAMI);
        assertEquals(15, tsunami.getDamage());
        assertEquals(OffensiveMagic.SpellType.TSUNAMI, tsunami.getSpellType());
        assertEquals(Element.WATER, tsunami.getElement());
        assertEquals(5, tsunami.getMPCost());
    }

    @Test
    void fireballMagicNonDefaultMPConstructorWorks() {
        OffensiveMagic fireball = new OffensiveMagic(OffensiveMagic.SpellType.FIREBALL, 20);
        assertEquals(10, fireball.getDamage());
        assertEquals(OffensiveMagic.SpellType.FIREBALL, fireball.getSpellType());
        assertEquals(Element.FIRE, fireball.getElement());
        assertEquals(20, fireball.getMPCost());
    }

    @Test
    void DragonConstructorWorks() {
        OffensiveMagic fireball = new OffensiveMagic(OffensiveMagic.SpellType.FIREBALL, 20);
        assertEquals(10, fireball.getDamage());
        assertEquals(OffensiveMagic.SpellType.FIREBALL, fireball.getSpellType());
        assertEquals(Element.FIRE, fireball.getElement());
        assertEquals(20, fireball.getMPCost());
    }


    @Test
    void negativeMPCostThrowsIAE() {
        assertThrows(IllegalArgumentException.class, () -> {
            new OffensiveMagic(OffensiveMagic.SpellType.FIREBALL, -10);
        });
    }
}

