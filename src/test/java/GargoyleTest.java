import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 * This class is not being implemented. Is rather serves as a template allowing for the class to developed in the future.
 */

public class GargoyleTest {


    @Test
    void resistanceOrWeaknessFilterTest() {
        Gargoyle defaultGargoyle = new Gargoyle("Default Goyle", 35, 30, 10);
        assertEquals(1, defaultGargoyle.resistanceOrWeaknessFilter(new OffensiveMagic(OffensiveMagic.SpellType.BLIZZARD,10)));
    }
}