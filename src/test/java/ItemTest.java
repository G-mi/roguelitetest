import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemTest {

//this test is for all Itemtypes


    @Test
    void weaponTest() {
        Item.Weapon testWeapon = new Item(Item.Weapon.Staff).getWeapon();

        assertEquals(Item.Weapon.class, testWeapon.getClass());


    }

    @Test
    void armourTest() {

        Item.Armour testArmour = new Item(Item.Armour.IronArmour).getArmour();
        assertEquals(Item.Armour.IronArmour, testArmour);

    }

    @Test
    void potionTest() {
        Item.Potion testPotion = new Item(Item.Potion.HealthPotion).getPotion();
        assertEquals(Item.Potion.HealthPotion, testPotion);

    }

    @Test
    void getTypeTest() {
        Item potionTest = new Item(Item.Potion.HealthPotion);
        Item weaponTest = new Item(Item.Weapon.Staff);
        Item armourTest = new Item(Item.Armour.OrichalcumArmour);
        assertEquals(Item.Potion.HealthPotion.name(), potionTest.getType());
        assertEquals(Item.Armour.OrichalcumArmour.name(), armourTest.getType());
        assertEquals(Item.Weapon.Staff.name(), weaponTest.getType());
        assertEquals("ERROR", new Item().getType());
    }

    @Test
    void getArmourRatingTest() {
        Item test = new Item(Item.Armour.IronArmour);
        assertEquals(10, test.getArmour().getArmourRating());
    }

    @Test
    void getWeaponDamageTest() {
        Item test = new Item(Item.Weapon.Spear);
        assertEquals(30, test.getWeapon().getDamageRating());
    }

    @Test
    void getPotionRestorationAmountTest() {
        Item test = new Item(Item.Potion.ManaPotion);
        assertEquals(50, test.getPotion().getRestorationAmount());
    }

    @Test
    void getPotionRestorableAttributeTest() {
        Item test = new Item(Item.Potion.StaminaPotion);
        assertEquals("Stamina", test.getPotion().getRestorableAttribute());
    }
/*
    @Test
    void toStringTest() {
        assertEquals("Spear", new Item(Item.Weapon.Spear).toString());
        assertEquals("IronArmour", new Item(Item.Armour.IronArmour).toString());
        assertEquals("StaminaPotion", new Item(Item.Potion.StaminaPotion).toString());
        assertEquals("", new Item().toString());
    }
*/

}