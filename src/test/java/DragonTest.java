import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class DragonTest {

    private Dragon defaultDragon;
    private Character defaultHero;
    private OffensiveMagic windSpell;
    private DataBase defaultDataBase;

    @BeforeEach
    void prel() {
        defaultDragon = new Dragon("Illidan", 50, 80, 100);
        
        defaultHero = new Priest("A", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN);
        Character defaultHeroTwo = new Warrior("B", 0,  new Ally.Equipment(Item.Armour.OrichalcumArmour, Item.Weapon.Spear), Ally.Race.DWARF);
        defaultDataBase = DataBase.getInstance();
        defaultDataBase.addToList(defaultHero, defaultHeroTwo);
        windSpell = new OffensiveMagic(OffensiveMagic.SpellType.WIND);
    }
    
    @AfterEach
    void done() {
        defaultDataBase.flush();
    }

    @Test
    void dragonFireBallCorrectDamageTest() {
        defaultDragon.throwFireBall(defaultHero);
        assertEquals(40, defaultHero.getCurrentHP());
    }

    @Test
    void dragonFireBallToNullCharacterTest() {
        assertThrows(IllegalArgumentException.class, () -> defaultDragon.throwFireBall(null));
    }

    @Test
    void receiveOffensiveFireSpellCorrectDamage() {
        defaultDragon.receiveOffensiveSpell(new OffensiveMagic(OffensiveMagic.SpellType.FIREBALL));
        assertEquals(45, defaultDragon.getCurrentHP());
    }

    // Earth type offensive spells apart from Earthquake cannot be currently tested.
    // That's because there is no other Earth element except from that.
    // However that would be repeating this same process
    @Test
    void receiveOffensiveEarthSpellCorrectDamage() {
        defaultDragon.receiveOffensiveSpell(new OffensiveMagic(OffensiveMagic.SpellType.EARTHQUAKE));
        assertEquals(50, defaultDragon.getCurrentHP());
    }

    @Test
    void receiveOffensiveIceSpellCorrectDamage() {
        defaultDragon.receiveOffensiveSpell(new OffensiveMagic(OffensiveMagic.SpellType.BLIZZARD));
        assertEquals(47, defaultDragon.getCurrentHP());
    }

    /**
     * Water type offensive spells apart from Tsunami cannot be currently tested.
     * That's because there is no other Water element except from that.
     * However that would be repeating this same process
     */
    @Test
    void receiveOffensiveWaterSpellCorrectDamage() {
        defaultDragon.receiveOffensiveSpell(new OffensiveMagic(OffensiveMagic.SpellType.TSUNAMI));
        assertEquals(50, defaultDragon.getCurrentHP());
    }

    @Test
    void receiveOffensiveWindSpellCorrectDamage() {
        defaultDragon.receiveOffensiveSpell(windSpell);
        assertEquals(20, defaultDragon.getCurrentHP());
    }

    @Test
    void receiveOffensiveSpellNullSpell() {
        assertThrows(IllegalArgumentException.class, () -> defaultDragon.receiveOffensiveSpell(null));
    }

    @Test
    void multiFireBallAttackTest() {
        ArrayList<Ally> targets = new ArrayList<Ally>(){{
            add(new Priest("HumanPriest", 0,  new Ally.Equipment(Item.Armour.LeatherArmour, Item.Weapon.Staff), Ally.Race.HUMAN));
            add(new Warrior("DwarfWarrior", 0,  new Ally.Equipment(Item.Armour.OrichalcumArmour, Item.Weapon.Spear), Ally.Race.DWARF));
            add(new Mage("ElfMage",0, new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff), Ally.Race.ELF));
            add(new Rogue("ElfRogue",0, new Ally.Equipment(Item.Armour.SteelArmour, Item.Weapon.Sword), Ally.Race.ELF));
        }};
        defaultDragon.throwMultiFireBall(targets);
        for (Character target : targets) {
            assertEquals(target.getMaxHp() - OffensiveMagic.SpellType.FIREBALL.defaultDamage, target.getCurrentHP());
        }
    }

    @Test
    void freezeDamageTest() {
        defaultDragon.castBlizzard(defaultHero);
        assertEquals(45, defaultHero.getCurrentHP());
    }

    @Test
    void ironTailAttackTargetNPE() {
        assertThrows(IllegalArgumentException.class, () -> defaultDragon.ironTailAttack(null));
    }

    @Test
    void ironTailAttackDamage() {
        defaultDragon.ironTailAttack(defaultHero);
        assertEquals(25, defaultHero.getCurrentHP());
    }

    @Test
    void aiTesting() {
        int i = defaultDragon.aiAttack();
       boolean test = i < 4 && i >= 0;
        defaultDragon.aiAttack();
        assertTrue(test);
    }


//    @Test
//    void killingMonsterTest() {
//        dragonDefault.receiveOffensiveSpell(windSpell);
//        dragonDefault.receiveOffensiveSpell(windSpell);
//        assertFalse(dragonDefault.getLifeStatus());
//    }



    @Test
    void dropsCorrectlyTest() {
        ArrayList<Item> droppable = defaultDragon.getDrop().getDroppables();

        ArrayList<Item> dropped = defaultDragon.getDrop().getDrops(new Random(){
            public int nextInt(int i){
                return i-1;
            }
        });
        Collections.reverse(droppable);
        assertEquals(2, dropped.size());
        assertEquals(droppable.get(0), dropped.get(0));
        assertEquals(droppable.get(1), dropped.get(1));
    }

    @Test
    void correctDropsForDragonTest(){

        Monster.Drop drops = defaultDragon.getDrop();

        ArrayList<Item> dropped = drops.getDrops(new Random() {
            public int nextInt(int i) {
                return i - 1;
            }
        });
        ArrayList<Item> droppable = defaultDragon.getDrop().getDroppables();


        Collections.reverse(droppable);
        assertEquals(2, dropped.size());
        assertEquals(droppable.get(0), dropped.get(0));
        assertEquals((droppable.get(1)), dropped.get(1));

    }
}
