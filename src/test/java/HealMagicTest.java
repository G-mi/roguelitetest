import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class HealMagicTest {

    private HealMagic heal;
    Ally.Equipment equip = new Ally.Equipment(Item.Armour.IronArmour, Item.Weapon.Staff);

    @BeforeEach
    void prel() {
        heal = new HealMagic();
    }

    @Test
    void nameAndLevelIsSetToDefaultByConstructor() {
        assertEquals(1, heal.getLevel());
        assertEquals("Heal", heal.getName());
        assertEquals(5, heal.getMPCost());
    }

    @Test
    void useHealOnSelfWorks() {
        Priest test = new Priest("default", 0, equip, Ally.Race.HUMAN);
        test.takeDamage(15);
        heal.useHeal(test, test);
        assertEquals(45, test.getCurrentHP());
        assertEquals(15, test.getCurrentMP());
        assertEquals(1, heal.getUses());
    }

    @Test
    void useHealOnOtherCharacterWorks() {
        Priest test = new Priest("default", 0, equip, Ally.Race.HUMAN);
        Priest test2 = new Priest("default", 0, equip, Ally.Race.HUMAN);
        test2.takeDamage(15);
        heal.useHeal(test2, test);
        assertEquals(45, test2.getCurrentHP());
        assertEquals(15, test.getCurrentMP());
        assertEquals(1, heal.getUses());
    }

    @Test
    void levelUpHealWorks() {
        Priest test = new Priest("default", 0, equip, Ally.Race.HUMAN);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        assertEquals(3, heal.getLevel());
        assertEquals(30, heal.calculateHealAmount());
    }

    @Test
    void cantLevelUpHealBeyondMaxWorks() {
        Priest test = new Priest("default", 0, equip, Ally.Race.HUMAN);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        heal.useHeal(test, test);
        assertEquals(5, heal.getLevel());
        assertEquals(50, heal.calculateHealAmount());
        assertEquals(8, heal.getUses());
    }
}




